# R2-file-browsing-worker

Cloudflare worker that allows to browse an R2 bucket using a web browser. Comparable to Apache files and directories browsing feature.

An object in an R2 bucket can be publicly viewed from a web browser. However, it is not possible to navigate within the bucket, so this worker exposes the content to navigation.

It will only process http GET and HEAD methods - all other methods are protected and dedicated to the Linux mirror synchronisation.

The use case that motivated the development of this worker is the hosting of Linux mirrors using the Cloudflare infrastructure.

| Note                                                                                                      |
|:----------------------------------------------------------------------------------------------------------|
| Since links cannot be used for this type of storage, the use case of Linux mirrors is not practical.      |


*HEAD method header sample**

```text
content-length: 45283
etag: 244320a98692e08fc860f889b61f9616
checksum-md5: 244320a98692e08fc860f889b61f9616
httpetag: "244320a98692e08fc860f889b61f9616"
```

## Initial setup

* Copy wrangler.toml.example as wrangler.toml
* Create an R2 bucket
* Create a KV namespace
* Update package.json
* Configure properties into wrangler.toml see wrangler.toml.example 

```toml 
name = "the name of your worker"
main = "src/index.ts"
compatibility_date = "2023-09-04"
account_id = "your account id"
workers_dev = true

[vars]
TITLE = "Html page title"

[[kv_namespaces]]
binding = "KV_NAMESPACE"
id = ""

[[r2_buckets]]
binding = "MY_BUCKET"
bucket_name = "artix-linux"
preview_bucket_name = "artix-linux" # to use a test bucket during dev
  ```
* [Cloudflare documentation](https://developers.cloudflare.com/r2/api/workers/workers-api-usage/)

### Dev
```bash
npm install
```

```bash
npm run start
```

### Deploy
```bash
npx wrangler deploy 
```