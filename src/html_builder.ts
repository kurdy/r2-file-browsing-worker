/**
 * Builder for html pages index.html and report.html
 * license: BSD-3-Clause
 */
import { Env, Content, SyncInfo, getLastSyncData, getSyncDatas } from '.';

function build_html_start(env: Env, pathname?: string) {
	const root_anchor: string = pathname === '/' && !undefined ? '</div>' : '<a href="/">🖚 Root</a>';
	return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${env.TITLE}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="shortcut icon" href="#" type="image/x-icon">
    <style>
      :root {
        font-family: sans-serif;
        line-height: 1.5;
        font-size: 1rem;
        font-weight: 400;
      }

      body {
        margin: 2px;
        min-width: 320px;
        min-height: 100vh;
      }

      h1 {
        font-size: 2em;
        line-height: 1.1;
        text-align: center;
      }
      h2 {
        font-size: 1.7em;
        line-height: 1.1;
        text-align: center;
      }

      a {
        font-family: monospace;
      }

			code {
        font-family: monospace;
      }

      main {
        width: 98vw;
        margin-left: auto;
        margin-right: auto;
     
      }
      table {
        width: 100%;
        border-width: 2px;
        border-spacing: 0px;
        border-style: solid;
        border-color: darkblue;
        margin: 3px;
      }
      th{
        text-align: center;
        background-color:ghostwhite;
      }
      td {
        text-align: center;
        border-width: 1px;
        border-spacing: 0px;
        border-style: dashed;
        border-color: gainsboro;
      }
      footer {
        display: flex;
        flex-direction: row;
        background-color:gainsboro;
        width: 98vw;
        margin-left: auto;
        margin-right: auto;
        justify-content: space-around
      }
      footer div {
        margin-left: 5px;
        flex-grow: 0;
        font-size: 0.8rem;
        font-style: italic;
      }
      footer div span{
        font-weight: bold;
      }
  </style>
  </head>
  <body>
    <main>
      <h1>${env.TITLE}</h1>
			${root_anchor}
      <table>`;
}

function build_html_th(parent: string) {
	const parent_anchor: string = parent === '' ? '</div>' : `<a href="${parent}">..</a>`;
	return `<tr>
		<th>${parent_anchor}</th>
		<th>Name</th>
		<th>Type</th>
		<th>Size</th>
		<th>Checksum</th>
		<th>Date</th>
	</tr>`;
}

function build_html_th_report() {
	return `<tr>
		<th>Total objects</th>
		<th>Total size</th>
		<th>Synchronization start</th>
		<th>Synchronization end</th>
		<th>Synchronization status</th>
		<th>Synchronization source</th>
	</tr>`;
}

function build_html_td_report(syncInfo: SyncInfo) {
	return `<tr>
		<td>${syncInfo.totalObjects.toLocaleString()}</td>
		<td>${syncInfo.totalSize.toLocaleString()}</td>
		<td>${syncInfo.startDate.toLocaleString()}</td>
		<td>${syncInfo.endTime.toLocaleString()}</td>
		<td>${syncInfo.isSyncCmdExitCodeOk ? '🗸' : '🗴'}</td>
		<td>${syncInfo.source}</td>
	</tr>`;
}

function build_html_td(content: Content | null) {
	let result = '';
	if (content) {
		let symbol = '🗋';
		if (content.isDir) {
			content.date = undefined;
			content.size = undefined;
			content.mime = 'Directory';
			symbol = '🖿';
			content.checksum = undefined;
		}
		const href = `${content.path}`;
		result = `
      <tr>
        <td><code>${symbol}</code></td>
        <td><a href='${href}'>${content.name}</a></td>
        <td>${content.mime || '-'}</td>
        <td><code>${content.size || '-'}<code></td>
        <td><code>${content.checksum || '-'}</code></td>
        <td>${content.date || '-'}</td>
      </tr>
    `;
	} else {
		result = `
      <tr>
        <td><code>😬</code></td>
        <td><a href='/>/</a></td>
        <td>Empty</td>
        <td><code>-<code></td>
        <td><code>-</code></td>
        <td>${new Date().toLocaleDateString()}</td>
      </tr>
    `;
	}
	return result;
}

function build_html_footer(info?: SyncInfo) {
	if (info) {
		return `</table>    
          </main>
          <footer>
            <div>Total objects: <span>${info.totalObjects.toLocaleString('fr-CH')}</span></div>
            <div>Total size: <span>${info.totalSize.toLocaleString('fr-CH')}</span> bytes</div>
            <div>Synchronization start: <span>${info.startDate.toLocaleString('fr-CH')}</span></div>
            <div>Synchronization end: <span>${info.endTime.toLocaleString('fr-CH')}</span></div>
            <div>Synchronization status: <span>${info.isSyncCmdExitCodeOk ? '🗸' : '🗴'}</span></div>
            <div>Synchronization source: <span>${info.source}</span></div>
            <div>report.<a href='/report.html'>html</a> <a href='/get_sync_info'>json</a></div>
          </footer>`;
	} else {
		return `</table>    
          </main>`;
	}
}

function build_html_end() {
	return `</body>
    </html>`;
}

/* Sample of Rclone object
{
    "Path": "state",
    "Name": "state",
    "Size": 259,
    "MimeType": "application/octet-stream",
    "ModTime": "2023-09-20T04:28:35.571345300+02:00",
    "IsDir": false,
    "Tier": "STANDARD"
  },
  {
    "Path": "testing",
    "Name": "testing",
    "Size": 0,
    "MimeType": "inode/directory",
    "ModTime": "2000-01-01T00:00:00.000000000Z",
    "IsDir": true
  },

*/

function toRCloneObjects(source: Array<any>): Array<Content> {
	const result: Array<Content> = [];
	source.forEach((element) => {
		const content: Content = {
			path: element.Path,
			name: element.Name,
			mime: element.MimeType,
			size: element.Size,
			checksum: undefined,
			date: element.ModTime,
			isDir: element.IsDir
		};
		result.push(content);
	});
	return result;
}

// As the root page can be time consuming we use a pregenerated data structure
// from synchonization process see POST for set_root_ls
export async function getRootPage(env: Env): Promise<string> {
	const root_index: Array<any> | null = await env.KV_NAMESPACE.get('key_root_listing', { type: 'json' });
	let contents: Array<Content> = [];
	if (root_index) {
		contents = contents.concat(toRCloneObjects(root_index));
	} else {
		console.error('No content found for root. The KV key_root_listing is missing. See script set_root_ls.bash');
	}
	return getPage('/', env, contents);
}

export async function getReportPage(env: Env): Promise<string> {
	const htmlPage: Array<string> = [];
	htmlPage.push(build_html_start(env));
	htmlPage.push(build_html_th_report());
	const datas = await getSyncDatas(env);
	datas.forEach((data) => {
		htmlPage.push(build_html_td_report(data));
	});
	htmlPage.push(build_html_footer());
	htmlPage.push(build_html_end());
	return htmlPage.join('');
}

export async function getPage(pathname: string, env: Env, contents: Array<Content>): Promise<string> {
	const dirs = contents
		.filter((e) => {
			return e.isDir;
		})
		.sort((a, b) => {
			return a.name.localeCompare(b.name);
		});
	const files = contents
		.filter((e) => {
			return !e.isDir;
		})
		.sort((a, b) => {
			return a.name.localeCompare(b.name);
		});
	const path = pathname || '/';
	const parent = path === '/' ? '' : `/${path.substring(0, pathname.lastIndexOf('/'))}` || '/';
	const htmlPage: Array<string> = [];
	htmlPage.push(build_html_start(env, path));
	htmlPage.push(build_html_th(parent));
	if (contents) {
		dirs.forEach((content) => {
			htmlPage.push(build_html_td(content));
		});
		files.forEach((content) => {
			htmlPage.push(build_html_td(content));
		});
	}
	htmlPage.push(build_html_footer(await getLastSyncData(env)));
	htmlPage.push(build_html_end());
	return htmlPage.join('');
}
export default getPage;
