/**
 * Cloudflare worker that allows to browse an R2 bucket using a web browser.
 * Some responses are specific to linux mirrors purpose
 * license: BSD-3-Clause
 */
import { getPage, getRootPage, getReportPage } from './html_builder';
export interface Env {
	// Example binding to R2. Learn more at https://developers.cloudflare.com/workers/runtime-apis/r2/
	MY_BUCKET: R2Bucket;
	TITLE: string;
	KV_NAMESPACE: KVNamespace;
	X_Custom_PSK: string;
	NO_CACHE_PATTERN : [string]
}

export interface Content {
	path: string;
	isDir: boolean;
	name: string;
	mime?: string;
	size?: number;
	checksum?: string;
	date?: string;
}

export interface SyncInfo {
	totalObjects: number;
	totalSize: number;
	startDate: Date;
	endTime: Date;
	isSyncCmdExitCodeOk: boolean;
	source: string;
}
async function build_contents(objectName: string, env: Env): Promise<Array<Content>> {
	const objectParent: string = objectName;
	//const objectParent: string = objectName.substring(0,objectName.lastIndexOf('/'))
	let truncated = true;
	let cursor = undefined;

	const files: Map<string, Content> = new Map();
	const dirs: Map<string, Content> = new Map();
	do {
		// Setup options for R2 Objects List
		const options: R2ListOptions = {
			prefix: objectParent,
			cursor: cursor,
			include: ['httpMetadata']
		};
		// Get a list of all objects, empty directories aren't included we get maximu of 1'000 items
		const listing = await env.MY_BUCKET.list(options);
		truncated = listing.truncated; // true means we should call again the next objects
		cursor = truncated ? listing.cursor : undefined;

		listing.objects.forEach((object) => {
			let key = object.key;

			if (objectParent) {
				key = object.key.replace(`${objectParent}/`, '');
			}

			const item = key.split('/');
			if (item.length === 1) {
				// It's a file
				const content: Content = {
					isDir: false,
					path: objectName ? `/${objectName}/${item[0]}` : `./${item[0]}`,
					name: `${item[0]}`,
					mime: `${object.httpMetadata?.contentType}`,
					size: object.size,
					checksum: `md5: ${hex(object.checksums.md5)}`,
					date: `${object.uploaded}`
				};
				files.set(item[0], content);
			} else {
				// It's a directory
				const content: Content = {
					isDir: true,
					path: objectName ? `/${objectName}/${item[0]}` : `./${item[0]}`,
					name: `${item[0]}`,
					mime: 'Directory'
				};
				dirs.set(item[0], content);
			}
		});
	} while (truncated);
	const contents: Array<Content> = [];
	for (const c of dirs.values()) {
		contents.push(c);
	}
	for (const c of files.values()) {
		contents.push(c);
	}
	return contents;
}

// create an hex string from arrayBuffer
function hex(arrayBuffer: ArrayBuffer | undefined): string {
	if (arrayBuffer) {
		return Array.prototype.map.call(new Uint8Array(arrayBuffer), (n) => n.toString(16).padStart(2, '0')).join('');
	} else {
		return '';
	}
}

// Some requests needs a key
function check_PSK(env: Env, headers: Headers): Response | void {
	const psk = headers.get('X-Custom-PSK');
	if (psk !== env.X_Custom_PSK) {
		console.error(`set_root_ls invalid key: ${psk}`);
		return new Response(`set_root_ls invalid key`, {
			status: 403
		});
	}
}
function toSyncInfoObject(source: any): SyncInfo {
	const syncInfo: SyncInfo = {
		totalObjects: Number.parseInt(source.total_objects),
		totalSize: Number.parseInt(source.total_size),
		startDate: new Date(Date.parse(source.start_date)),
		endTime: new Date(Date.parse(source.start_date)),
		isSyncCmdExitCodeOk: source.sync_cmd_exit_code === 0 ? true : false,
		source: source.source
	};
	return syncInfo;
}

export async function getLastSyncData(env: Env): Promise<SyncInfo> {
	const kv_list = await env.KV_NAMESPACE.list({ prefix: 'key_report_row_' });
	const last = kv_list.keys
		.sort((a, b) => {
			const left: number = Number.parseInt(a.name.slice(a.name.lastIndexOf('_')));
			const right: number = Number.parseInt(b.name.slice(b.name.lastIndexOf('_')));
			return left - right;
		})
		.pop()?.name;
	return toSyncInfoObject(await env.KV_NAMESPACE.get(last || '', { type: 'json' }));
}

export async function getSyncDatas(env: Env): Promise<Array<SyncInfo>> {
	const data: Array<SyncInfo> = [];
	const kv_list = await env.KV_NAMESPACE.list({ prefix: 'key_report_row_' });
	for (const k of kv_list.keys) {
		const syncInfo = toSyncInfoObject(await env.KV_NAMESPACE.get(k.name, { type: 'json' }));
		data.push(syncInfo);
	}
	const result = data.sort((a, b) => {
		return b.endTime.getTime() - a.endTime.getTime();
	});
	return result;
}

/**
 * Entry of requests
 */
export default {
	async fetch(request: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
		const url = new URL(request.url);
		const objectName = url.pathname.slice(1);
		console.log(env.NO_CACHE_PATTERN);

		if (objectName === 'favicon.ico') {
			return new Response(null, {
				status: 204
			});
		}

		/**
		 * POST
		 */

		if (objectName === 'set_root_ls' && request.method === 'POST') {
			const nok = check_PSK(env, request.headers);
			if (nok) {
				return nok;
			}
			const contentType = request.headers.get('content-type');
			if (contentType && contentType.includes('application/json')) {
				const root_ls = JSON.stringify(await request.json());
				if (!(root_ls.startsWith('[') && root_ls.endsWith(']'))) {
					return new Response(`Bad request content. ${root_ls}"`, {
						status: 400
					});
				} else {
					await env.KV_NAMESPACE.put('key_root_listing', root_ls, {
						expirationTtl: 12 * 3600
					});
					return new Response('key_root_listing created', {
						status: 201
					});
				}
			} else {
				return new Response(`Bad request invalid content-type`, {
					status: 400
				});
			}
		}

		if (objectName === 'add_report_row' && request.method === 'POST') {
			const nok = check_PSK(env, request.headers);
			if (nok) {
				return nok;
			}
			const contentType = request.headers.get('content-type');
			if (contentType && contentType.includes('application/json')) {
				const row = JSON.stringify(await request.json());
				if (!row) {
					return new Response(`Bad request content."`, {
						status: 400
					});
				} else {
					await env.KV_NAMESPACE.put(`key_report_row_${Date.now()}`, row, {
						expirationTtl: 30 * 24 * 3600
					});
					return new Response('Report row added', {
						status: 201
					});
				}
			} else {
				return new Response(`Bad request invalid content-type`, {
					status: 400
				});
			}
		}

		/**
		 * GET without CACHE
		 */
		if (objectName === 'get_bucket_list' && request.method === 'GET') {
			const nok = check_PSK(env, request.headers);
			if (nok) {
				return nok;
			}
			const truncated = true;
			let cursor = undefined;
			let objects: Array<R2Object> = [];
			do {
				// Setup options for R2 Objects List
				const options: R2ListOptions = {
					prefix: undefined,
					//delimiter: 'RXTKRLJejE/',
					//limit: 1000,
					cursor: undefined,
					include: ['httpMetadata']
				};
				const listing = await env.MY_BUCKET.list(options);
				listing.truncated; // true means we should call again the next objects
				cursor = truncated ? listing.cursor : undefined;
				objects = objects.concat(listing.objects);
			} while (truncated);
			return new Response(JSON.stringify(objects), {
				headers: { 'content-type': 'text/json; charset=UTF-8' },
				status: 200
			});
		}

		// Manjaro mirror use state file to qualify mirror state
		if (objectName.endsWith('state') && request.method === 'GET') {
			const object = await env.MY_BUCKET.get(objectName);
			if (object) {
				const headers = new Headers();
				object.writeHttpMetadata(headers);
				headers.append('Last-Modified', object.uploaded.toUTCString());
				const response = new Response(object.body, {
					headers,
					status: 200
				});
				return response;
			} else {
				const response = new Response('{objectName} has not found.', {
					status: 404
				});
				return response;
			}
		}

		/**
		 * GET and HEAD (use cache)
		 */

		// Construct the cache key from the URL
		const cacheKey = new Request(url.toString(), request);
		const cache = caches.default;
		// Check whether the value is already available in the cache
		// if not, you will need to fetch it from origin, and store it in the cache
		const cacheResponse = await cache.match(cacheKey);
		if (cacheResponse) {
			return cacheResponse;
		}

		if (objectName === 'get_last_sync_info' && request.method === 'GET') {
			const result = await getLastSyncData(env);
			const headers = new Headers();
			headers.set('etag', result.endTime.getTime().toString());
			headers.append('Cache-Control', 's-maxage=600');
			headers.append('Last-Modified', result.endTime.toUTCString());
			headers.append('content-type', 'text/json; charset=UTF-8');
			const response = new Response(JSON.stringify(result), {
				headers: headers,
				status: 200
			});
			// Put response in cache
			ctx.waitUntil(cache.put(cacheKey, response.clone()));
			return response;
		}

		// Return Synchronization info's as JSON
		if (objectName === 'get_sync_info' && request.method === 'GET') {
			const result = await getSyncDatas(env);
			const headers = new Headers();
			headers.set('etag', result[0].endTime.getTime().toString());
			headers.append('Cache-Control', 's-maxage=600');
			headers.append('Last-Modified', result[0].endTime.toUTCString());
			headers.append('content-type', 'text/json; charset=UTF-8');
			const response = new Response(JSON.stringify(result), {
				headers: headers,
				status: 200
			});
			// Put response in cache
			ctx.waitUntil(cache.put(cacheKey, response.clone()));
			return response;
		}

		// Return Synchronization info's as HTML table
		if (objectName === 'report.html' && request.method === 'GET') {
			const lastSync = await getLastSyncData(env);
			const page = await getReportPage(env);
			const headers = new Headers();
			headers.set('etag', lastSync.endTime.getTime().toString());
			headers.append('Cache-Control', 's-maxage=600');
			headers.append('Last-Modified', lastSync.endTime.toUTCString());
			headers.append('content-type', 'text/html; charset=UTF-8');
			const response = new Response(page, {
				headers: headers,
				status: 200
			});
			// Put response in cache
			ctx.waitUntil(cache.put(cacheKey, response.clone()));
			return response;
		}

		// HEAD create response header with object informations
		if (request.method === 'HEAD') {
			const options: R2ListOptions = {
				prefix: objectName
			};
			const listing = await env.MY_BUCKET.list(options);
			if (listing) {
				const obj = listing.objects.find((o) => o.key === objectName);
				if (obj) {
					const headers = new Headers();
					obj.writeHttpMetadata(headers);
					headers.append('Last-Modified', obj.uploaded.toUTCString());
					const response = new Response(null, {
						headers: headers,
						status: 200
					});
					// Put response in cache
					ctx.waitUntil(cache.put(cacheKey, response.clone()));
					return response;
				} else {
					return new Response(`Object ${objectName} not found`, {
						status: 404
					});
				}
			} else {
				return new Response(`Could not get objects listing from R2`, {
					status: 500
				});
			}
		}

		// Manage Get Data
		let object = undefined;
		if (request.method === 'GET') {
			object = await env.MY_BUCKET.get(objectName);
		} else {
			return new Response(`Bad request: ${objectName}`, {
				status: 400
			});
		}

		if (object) {
			//return Response.redirect(`http://${env.R2_DOMAIN}${url.pathname}`, 301); //Bad performance
			// Object exist we just serve it
			const headers = new Headers();
			object.writeHttpMetadata(headers);
			headers.set('etag', object.httpEtag);
			headers.append('Cache-Control', 's-maxage=3600');
			headers.append('Last-Modified', object.uploaded.toUTCString());
			const response = new Response(object.body, {
				headers,
				status: 200
			});
			// Put response in cache
			ctx.waitUntil(cache.put(cacheKey, response.clone()));
			return response;
		} else {
			// Object doesn't exist we expect a directory
			if (objectName === 'index.html' || !objectName) {
				const now = new Date();
				const body = await getRootPage(env);
				const headers = new Headers();
				headers.set('etag', now.getMilliseconds().toString());
				headers.append('Cache-Control', 's-maxage=3600');
				headers.append('Last-Modified', now.toUTCString());
				headers.append('content-type', 'text/html; charset=UTF-8');
				const response = new Response(body, {
					headers,
					status: 200
				});
				// Put response in cache
				ctx.waitUntil(cache.put(cacheKey, response.clone()));
				return response;
			} else {
				const contents = await build_contents(objectName, env);
				if (contents.length > 0) {
					const now = new Date();
					const body = await getPage(objectName, env, contents);
					const headers = new Headers();
					headers.set('etag', now.getMilliseconds().toString());
					headers.append('Cache-Control', 's-maxage=3600');
					headers.append('Last-Modified', now.toUTCString());
					headers.append('content-type', 'text/html; charset=UTF-8');
					const response = new Response(body, {
						headers,
						status: 200
					});
					// Put response in cache
					ctx.waitUntil(cache.put(cacheKey, response.clone()));
					return response;
				}
				// Finally it isn't a directory or a existing object
				return new Response(`${objectName} not found.`, {
					status: 404
				});
			}
		}
	}
};
